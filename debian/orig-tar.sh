#!/bin/sh -e

ZIP=$3
TAR=../libisnativec-java_$2.orig.tar.gz
DIR=libisnativec-java-$2

unzip $ZIP
mv distribution $DIR
tar -c -z -f $TAR --exclude '*.jar' --exclude '*.zip' --exclude '*/javadoc/*' $DIR
rm -rf $DIR $ZIP

# move to directory 'tarballs'
if [ -r .svn/deb-layout ]; then
  . .svn/deb-layout
  mv $TAR $origDir
  echo "moved $TAR to $origDir"
fi

