package com.sun.jna;

public class JNATools {

	public static long getPeer(Pointer p) {
		if (p == null) {
			return 0;
		}
		return p.peer;
	}
}
